var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");
var chance = new require('chance')();
var avatar = require('avatar-generator')({
    convert: 'D:/Apps/ImageMagick-6.9.3-Q16/convert'
});

describe('Testing strongloop\'s uploading feature', function () {

    after("ensure logout", function (done) {
        apiUtils.logout(done);
    });

    it('should upload a file random generated', function (done) {
        apiUtils.login({
            username: 'foo',
            password: 'bar'
        }, function () {
            var containerId = apiUtils.getUserId();
            avatar('1', 'male', 400)
                .write("./img/image.jpg", function (err) {
                    apiUtils.endPointRoot('ProfileContainers');
                    apiUtils.authRequest({
                        verb: 'post',
                        url: '/' + containerId + '/upload',
                        attach: require('path').join(__dirname, '/img/image.jpg')
                    }, null, done);
                });
        });
    });

    it('should not allow a user to upload a picture for other profile profile', function (done) {
        avatar('1', 'male', 400)
            .write("./img/image.jpg", function (err) {
                apiUtils.endPointRoot('ProfileContainers');
                apiUtils.authRequest({
                    verb: 'post',
                    url: '/' + 2 + '/upload',
                    attach: require('path').join(__dirname, '/img/image.jpg'),
                    expect: 403
                }, null, done);
            });
    });
});