var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");
var chance = new require('chance')();


var DENY_CODE = 401;

describe("Testing meeting functionality", function () {

    after("ensure logout", function (done) {
        apiUtils.logout(done);
    });

    it('should not allow to post a meeting unauthenticated', function (done) {
        apiUtils.authRequest({
            verb: 'post',
            url: '/1/meetings',
            body: {
                date: Date.now(),
                location: chance.address(),
                city: chance.city(),
                details: chance.paragraph()
            },
            //TODO: when launching all tests accross I'm getting 403; If testing just this file 401
            expect: DENY_CODE
        }, null, done);
    });

    it('should not allow to post a meeting for othe user', function (done) {
        apiUtils.login({
            username: 'jo',
            password: 'jo'
        }, function () {
            apiUtils.authRequest({
                verb: 'post',
                url: '/1/meetings',
                body: {
                    date: Date.now(),
                    location: chance.address(),
                    city: chance.city(),
                    details: chance.paragraph()
                },
                expect: 403
            }, null, done);
        });
    });

    it('should not allow to post a meeting if user is not patient', function (done) {
        apiUtils.login({
            username: 'will',
            password: 'will'
        }, function () {
            apiUtils.authRequest({
                verb: 'post',
                url: '/6/meetings',
                body: {
                    date: Date.now(),
                    location: chance.address(),
                    city: chance.city(),
                    details: chance.paragraph()
                },
                expect: 401
            }, null, done);
        });
    });

    it('should allow to post a meeting if user is patient (jo 2 meetings)', function (done) {
        apiUtils.login({
            username: 'jo',
            password: 'jo'
        }, function () {
            apiUtils.authRequest({
                verb: 'post',
                url: '/' + apiUtils.getUserId() + '/meetings',
                body: {
                    date: Date.now(),
                    location: chance.address(),
                    city: chance.city(),
                    details: chance.paragraph()
                }
            }, function () {
                apiUtils.authRequest({
                    verb: 'post',
                    url: '/' + apiUtils.getUserId() + '/meetings',
                    body: {
                        date: Date.now(),
                        location: chance.address(),
                        city: chance.city(),
                        details: chance.paragraph()
                    }
                }, null, done)
            });
        });
    });


    it('should allow a patient to see their meetings following\'s meetings', function (done) {
        apiUtils.authRequest({
            url: '/' + apiUtils.getUserId() + '/meetings',
        }, function (body) {
            assert.strictEqual(2, body.length, 'there must be two meetings for user jo');
            done();
        });
    });

    it('should allow to post a meeting if user is patient (bo 1 meeting)', function (done) {
        apiUtils.login({
            username: 'bo',
            password: 'bo'
        }, function () {
            apiUtils.authRequest({
                verb: 'post',
                url: '/' + apiUtils.getUserId() + '/meetings',
                body: {
                    date: Date.now(),
                    location: chance.address(),
                    city: chance.city(),
                    details: chance.paragraph()
                }
            }, null, done);
        });
    });


    var meetings;
    it('should allow a follower to see their following\'s meetings', function (done) {
        apiUtils.login({
            username: 'will',
            password: 'will'
        }, function () {
            apiUtils.authRequest({
                url: '/' + apiUtils.getUserId() + '/meetings',
            }, function (body) {
               // console.log(body);
                assert.strictEqual(3, body.length, 'there must be 3 meetings for user will (jo + bo\'s)');
                meetings = body;
                done();
            });
        });
    });

    it('should allow for a follower to attend a followee meeting', function (done) {
        var meetToAttend = meetings[0];
        apiUtils.endPointRoot("Meetings");
        apiUtils.authRequest({
            verb: 'post',
            url: '/' + meetToAttend.id + '/attenders',
            body: {
                userid: apiUtils.getUserId()
            }
        }, function () {
            apiUtils.authRequest({
                url: '/' + meetToAttend.id + '/attenders?filter={"include":{"relation":"diaLinkUser", "scope":{"fields":["username"]}}}'
            }, function (body) {
                assert.strictEqual(1, body.length);
                assert.strictEqual('will', body[0].diaLinkUser.username);
                done();
            });
        });
    });

    it('should allow for a follower to withdraw from a meeting', function (done) {
        var meetToAttend = meetings[0];
        apiUtils.endPointRoot("Meetings");
        apiUtils.authRequest({
            verb: 'delete',
            url: '/' + meetToAttend.id + '/attenders',
            body: {
                id: 1 // attender record id
            },
            expect: 204
        }, function () {
            var query = '?where={"meetingId":"' + meetToAttend.id + '","userid":"' + apiUtils.getUserId() + '"}';
            apiUtils.authRequest({
                url: '/' + meetToAttend.id + '/attenders/count' + query
            }, function (body) {
                assert.strictEqual(0, body.count);
                done();
            });
        });
    });

});
