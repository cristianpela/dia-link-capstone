var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");
var chance = new require('chance')();

describe('Testing the register feature', function () {

    var user = {
        username: chance.word(),
        password: chance.string({ length: 5 }),
        email: chance.email(),
        patient: chance.bool(),
        birthdate: chance.birthday(),
        city: chance.city(),
        country: chance.country({ full: true }),
        about: chance.paragraph({ sentences: 3 }),
        fullname: chance.name()
    };

    after("ensure logout", function (done) {
        apiUtils.logout(done);
    });

    it('should register a new user and then login', function (done) {
        apiUtils.authRequest({
            verb: 'post',
            body: user
        }, function () {
            apiUtils.login({
                username: user.username,
                password: user.password
            }, function (resBody) {
                assert.strictEqual(user.username, resBody.username);
                done();
            });
        });
    });
});