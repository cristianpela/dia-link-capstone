var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");
var chance = new require('chance')();

describe("Testing check in reporting feature", function () {

    /* these users and their relation are prerecored in memory db on server
       Will is following Jo
    */
    var userJoId = 2, // patient uname & pass is jo
        userWillId = 6; //non-patient uname & pass will

    var questions;

    beforeEach(function () {
        apiUtils.endPointRoot("DiaLinkUsers");
    });

    after("ensure logout", function (done) {
        apiUtils.logout(done);
    });

    it("Should return all questions", function (done) {
        apiUtils.endPointRoot("CheckInQuestions");
        apiUtils.authRequest(function (err, res) {
            assert(Array.isArray(res.body));
            assert.strictEqual(
                'What was your blood sugar level at [meal time/bedtime]?',
                res.body[0].body
            );
            questions = res.body;
            done();
        });
    });

    it("Should not allow user Will who is non-patient to add a report", function (done) {
        apiUtils.login({
            username: 'will',
            password: 'will'
        }, function () {
            var repUrl = "/" + apiUtils.getUserId() + "/checkInReports";
            apiUtils.authRequest({
                verb: 'post',
                url: repUrl,
                expect: 401
            }, null, done);
        });
    });
    
    it('Should allow will to send a check in request to jo.', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        var repUrl = "/" + 2 + "/send";
        apiUtils.authRequest({
            url: repUrl,
        }, null, done);
    });
    
     it('Should not allow will to see jo\'s check in requests.', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        apiUtils.authRequest({
            url: '/',
            expect: 403
        }, null, done);
    });

    it('Should allow user Jo who is patient to fill a report', function (done) {
        var report = function () {
            var report = { answers: [] };
            questions.forEach(function (question, index) {
                var answer = (index === 0) ? chance.natural({ min: 70, max: 250 }) : chance.word();
                report.answers.push({
                    question: question.body,
                    answer: answer
                });
            });
            return report;
        };
        apiUtils.login({
            username: 'jo',
            password: 'jo'
        }, function () {
            // post 2 reports
            apiUtils.authRequest({
                verb: 'post',
                body: report(),
                url: "/createCheckInReport"
            }, function (body) {
                 r = report();
                apiUtils.authRequest({
                    verb: 'post',
                    body: report(),
                    url: "/createCheckInReport"
                }, function (body) {
                    done();
                });
            });
        });
    });

    it('Should allow Jo to see his reports', function (done) {
        var repUrl = "/" + apiUtils.getUserId() + "/checkInReports";
        apiUtils.authRequest({
            url: repUrl
        }, function (body) { 
            done();
        });
    });
    
     it('Should not allow jo to send a check in request to himself.', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        var repUrl = "/" + apiUtils.getUserId() + "/send";
        apiUtils.authRequest({
            url: repUrl,
            expect: 403
        }, null, done);
    });
    
    it('Should  allow jo to see his check in requests', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        var repUrl = '/?filter={"where":{"target":2, "aknowledge":false}}';
        apiUtils.authRequest({
            url: repUrl
        }, function(body){
          assert.isAtLeast(1, body.length);
          done();  
        });
    });
    
     it('Should  allow jo acknowledge his checkin requests', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        var repUrl = '/update?where={"target":2}';
        apiUtils.authRequest({
            verb: 'post',
            url: repUrl,
            body:{
               aknowledge: true,
            }
        },null, done);
    });
    
     it('Should  allow jo to see his check in requests', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        var repUrl = '/?filter={"where":{"target":2, "aknowledge":false}}';
        apiUtils.authRequest({
            url: repUrl
        }, function(body){
          assert.strictEqual(0, body.length);
          done();  
        });
    });
    
    
     it('Should not allow jo to see other user checkin requests', function (done) {
        apiUtils.endPointRoot('CheckInRequests');
        var repUrl = '/?filter={"where":{"target":3, "aknowledge":false}}';
        apiUtils.authRequest({
            url: repUrl,
            expect: 403
        },null, done);
    });
    

    it('Should allow Jo to see his report details', function (done) {
        var repUrl = "/" + apiUtils.getUserId() + "/checkInReports/" + 1 + "/answers";
        apiUtils.authRequest({
            url: repUrl
        }, function (body) {
            done();
        });
    });

    it('Should allow Will to see Jo\'s reports', function (done) {
        apiUtils.login({
            username: 'will',
            password: 'will'
        }, function () {
            var repUrl = "/" + userJoId + "/checkInReports";
            apiUtils.authRequest({
                url: repUrl
            }, function (body) {
                done();
            });
        });
    });
    
 
    it('Should allow Will to see Jos report details', function (done) {
        var repUrl = "/" + userJoId + "/checkInReports/2/answers";
        apiUtils.authRequest({
            url: repUrl
        }, function (body) {
            done();
        });
    });

    it('should see glycemic data chart', function (done) {
         apiUtils.authRequest({
            url: '/'+userJoId+'/glycemicChartData/'
        }, function (body) {
             var firstEntry = body.dataChart[0];
             assert.isNotNull(firstEntry);
             assert.isNotNull(firstEntry.gi);
             assert.isNotNull(firstEntry.date);
            done();
        });
    });

    it('Should not allow Foo to see Jos reports. Fo is not following Jo', function (done) {
        apiUtils.login({
            username: 'foo',
            password: 'bar'
        }, function () {
            var repUrl = "/" + userJoId + "/checkInReports";
            apiUtils.authRequest({
                url: repUrl,
                expect: 403
            }, null, done);
        });
    });

    it('Should not allow Foo to see a report of Joe. Fo is not following Jo', function (done) {
        apiUtils.login({
            username: 'foo',
            password: 'bar'
        }, function () {
            var repUrl = "/" + userJoId + "/checkInReports/" + 1;
            apiUtils.authRequest({
                url: repUrl,
                expect: 403
            }, null, done);
        });
    });

    it('Should not allow Foo to see Jos report details', function (done) {
        var repUrl = "/" + userJoId + "/checkInReports/" + 1 + "/answers";
        apiUtils.authRequest({
            url: repUrl,
            expect: 403
        }, null, done);
    });
});