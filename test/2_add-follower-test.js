
var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");


describe('Testing capability to send a follow request and accepting it', function () {

    var followRequestId;

    after("ensure logout", function (done) {
        apiUtils.logout(done);
    });

    it("should login as user 4", function (done) {
        apiUtils.login({
            username: "mo",
            password: "bo"
        }, function (body) {
            assert.strictEqual(4, apiUtils.getUserId());
            done();
        });
    });

    it("should add a user 4 a friend request to user 1 request", function (done) {
        apiUtils.authRequest({
            url: '/1/addFollowRequest'
        },
            function (body) {
                assert.equal("Follow request sent!", body.addOk);
                done();
            });
    });

    it("should not allow to follow yourself", function (done) {
        apiUtils.authRequest({
            url: '/4/addFollowRequest',
            expect: 403
        }, null, done);
    });

    it("should not let user 4  to accept their follow request. Only user 1 is allowed", function (done) {
        apiUtils.endPointRoot('Follows');
        apiUtils.authRequest({
            verb: 'put',
            url: '/3',
            body: { accepted: true },
            expect: 403
        }, null, done);
    });

    it("should login as user 1", function (done) {
        apiUtils.login({
            username: "foo",
            password: "bar"
        }, function (body) {
            assert.strictEqual(1, apiUtils.getUserId());
            done();
        });
    });


    var followId;
    it("should list follow requests", function (done) {
        var url = '/?filter={"where":{"followeeId": 1, "accepted": false}, "include": {"relation": "follower", "scope":{"fields":["username","city","country"]}}}';
        apiUtils.authRequest({ url: url },
            function (body) {
                followId = body[0].id;
                assert.strictEqual(1, body.length);
                done();
            });
    });

    it("should accept follow request", function (done) {
        apiUtils.endPointRoot('Follows');
        apiUtils.authRequest({
            verb: 'put',
            url: '/'+followId,
            body: { accepted: true },
        }, null, done);
    });
    
    it("should have status as follower for user 4(mo), from user 1(jo) perspective,", function(done){
         apiUtils.authRequest({
             url: '/status/4'
         }, function(body){
            assert.strictEqual('follower', body.status[0].type);
            done(); 
         });
    });

    it("should list followers for user 1: [4]", function (done) {
        var url = '/?filter={"where":{"followeeId": 1, "accepted": true}, "include": {"relation": "follower", "scope":{"fields":["username","city","country"]}}}';
        apiUtils.authRequest({ url: url },
            function (body) {
                assert.strictEqual(1, body.length);
                assert.strictEqual(4, body[0].followerId);
                done();
            });
    });


    it("should list followings for user 4: [1]", function (done) {
        apiUtils.login({
            username: "mo",
            password: "bo"
        }, function (body) {
            var url = '/?filter={"where":{"followerId": 4, "accepted": true}, "include": {"relation": "followee", "scope":{"fields":["username","city","country"]}}}';
            apiUtils.authRequest({ url: url },
                function (body) {
                    assert.strictEqual(1, body.length);
                    assert.strictEqual(1, body[0].followeeId);
                    done();
                });
        });

    });
    
     it("should have status as followee, for user 1(jo), from user 4(mo) perspective", function(done){
         apiUtils.authRequest({
             url: '/status/1'
         }, function(body){
            assert.strictEqual('followee', body.status[0].type);
            done(); 
         });
    });

    it("should not allow user jo to remove user 4(mo) as following of 1(foo)", function (done) {
        apiUtils.login({
            username: "will",
            password: "will"
        }, function () {
            apiUtils.authRequest({
                verb: 'delete',
                url: '/'+followId,
                expect: 403
            }, null, done);
        });
    });


    it("should allow user 4 to remove a user 1 as following", function (done) {
        apiUtils.login({
            username: "mo",
            password: "bo"
        }, function () {
            apiUtils.authRequest({
                verb: 'delete',
                url: '/'+followId,
            }, null, done);
        });
    });
});