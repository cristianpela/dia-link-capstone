var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");
var chance = new require('chance')();


var DENY_CODE = 401;

describe("Testing event feature", function () {
    
    after("ensure logout", function(done){
       apiUtils.logout(done); 
    });

    it('should allow everyone to see events', function (done) {
        apiUtils.endPointRoot("Events");
        apiUtils.authRequest(done);
    });

    it('should not allow to post an event unauthenticated', function (done) {
        apiUtils.endPointRoot("DiaLinkUsers");
        apiUtils.authRequest({
            url: '/' + apiUtils.getUserId() + '/events',
            verb: 'post',
            body: {
                date: Date.now(),
                location: chance.address(),
                city: chance.city(),
                details: chance.paragraph()
            },
            //TODO: when launching all tests accross I'm getting 403; If testing just this file 401
            expect: DENY_CODE
        }, null, done);
    });

    it('should allow to post an event authenticated', function (done) {
        apiUtils.login({
            username: 'jo',
            password: 'jo'
        }, function () {
            apiUtils.endPointRoot("DiaLinkUsers");
            apiUtils.authRequest({
                url: '/' + apiUtils.getUserId() + '/events',
                verb: 'post',
                body: {
                    date: Date.now(),
                    location: chance.address(),
                    city: chance.city(),
                    details: chance.paragraph()
                }
            }, null, done);
        });
    });

});