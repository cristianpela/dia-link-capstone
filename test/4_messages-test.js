var assert = require('chai').assert;
var apiUtils = require('./utils.js')("DiaLinkUsers");

describe("Testing messaging feature", function () {

    var userJoId = 2, // patient uname & pass is jo
        userWillId = 6; //non-patient uname & pass will

    var message = "Hello World!";

    after("ensure logout", function (done) {
        apiUtils.logout(done);
    });

    it("Should send a message form Will to Jo", function (done) {
        apiUtils.login({
            username: 'will',
            password: 'will'
        }, function (body) {
            // send message;
            apiUtils.authRequest({
                verb: 'post',
                url: "/" + userJoId + '/messages',
                body: {
                    from: 'will',
                    title: message
                }
            }, function () {
                done();
            });
        });
    });

    it("Should not allow Will to see Jos messages", function (done) {
        apiUtils.authRequest({
            url: "/" + userJoId + '/messages',
            expect: 403
        }, null, done);
    });

    it("Should allow Jo to see His messages", function (done) {
        apiUtils.login({
            username: 'jo',
            password: 'jo'
        }, function (body) {
            // send message;
            apiUtils.authRequest({
                verb: 'get',
                url: "/" + userJoId + '/messages'
            }, function (body) {
                assert.strictEqual(message, body[0].title);
                done();
            });
        });
    });
});