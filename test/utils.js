var baseURL = "http://localhost:3000/api/";
var request = require('supertest'),
    api = request(baseURL);
var epr, token, userId;

module.exports = function utils(endPointRoot) {
    epr = endPointRoot;
    return {
        endPointRoot: function (endPointRoot) {
            epr = endPointRoot;
        },
        setToken: function (newToken) {
            token = newToken;
        },
        getToken: function () {
            return token;
        },
        setUserId: function (id) {
            userId = id;
        },
        getUserId: function () {
            return userId;
        },
        rootPath: function () {
            return baseURL + epr;
        },
        login: function (credentials, bodyCb) {
            this.json('post', 'DiaLinkUsers/login')
                .send(credentials)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;
                    token = res.body.id;
                    userId = res.body.userId;
                    if (bodyCb) bodyCb(res.body);
                });
        },
        logout: function (cb) {
            if(!token){
                cb();
                return;
            }
            this.authRequest({
                verb: 'post',
                overrideEpr: true,
                url: 'DiaLinkUsers/logout',
                token: token,
                expect: 204
            }, cb);
        },

        json: function (verb, url, contentType) {
            return api[verb](url)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/);
        },
        authRequest: function (info, cb, done) {
            if (typeof info === 'function' && !cb) {
                var cbFn = info; // info is a function instead of object with request data
                this.json('get', epr).expect(200).end(cbFn);
            } else {
                var infoUrl = info.url || '';
                var fullUrl = (info.overrideEpr) ? infoUrl : epr + infoUrl;

                var req = this.json(info.verb || 'get', fullUrl)
                if (token)
                    req = req.set('x-access-token', token);
                if (info.body)
                    req = req.send(info.body);
                if (info.query)
                    req = req.query(info.query);
                if (info.attach) {
                    req = req.set('Connection', 'keep-alive')
                        .attach('attachment', info.attach)
                }
                req.expect(info.expect || 200).end(done || function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    if (cb) cb(res.body);
                });
            }
        }
    }
}