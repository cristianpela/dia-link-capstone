var loopback = require('loopback'),
    async = require('async'),
    chance = new require('chance')(),
    avatarRequest = require('request'),
    fs = require('fs'),
    utils = require('./../utils/utils.js')();

module.exports = function (DiaLinkUser) {

    // DiaLinkUser.beforeRemote('**', function (context, inst, next) {
    //     console.log(context.method.name);
    //     next();
    // });

    /**
     * Hook that returns a user object after login with fields: id (auth-token), patient, usernamem and userdid
     */
    DiaLinkUser.afterRemote('login', function (context, user, next) {
        if (context.result) {
            DiaLinkUser.findOne(utils.idFilter(user.userId), function (err, user) {
                if (err) throw err;
                var answer = {
                    id: context.result.id,
                    patient: user.patient,
                    username: user.username,
                    userId: user.id
                };
                context.result = answer;
                next();
            });
        } else
            next();
    });

    /**
     * Hook for following "role" check
     */
    DiaLinkUser.afterRemote('*.__get__checkInReports', function (context, reports, next) {
        utils.checkIfUserIsFollowingPatient(DiaLinkUser, context, next);
    });

    /**
     * Hook for following "role" check
     */
    DiaLinkUser.afterRemote('*.__create__checkInRequests', function (context, reports, next) {
        utils.checkIfUserIsFollowingPatient(DiaLinkUser, context, next);
    });

    /**
     * Hook for following "role" check
     */
    DiaLinkUser.afterRemote('*.__findById__checkInReports', function (context, reports, next) {
        utils.checkIfUserIsFollowingPatient(DiaLinkUser, context, next);
    });

    /**
     * hook for nested urls like DiaLinkUsers/:id/checkInReports/:id/answers
     */
    DiaLinkUser.afterRemote('*.__get__checkInReports__answers', function (context, reports, next) {
        utils.checkIfUserIsFollowingPatient(DiaLinkUser, context, next);
    });

    /**
     * Hook for owner "role" check
     */
    DiaLinkUser.afterRemote('*.__get__messages', function (context, modelInstance, next) {
        //TODO: it looks like acl doesn't kick in. for now I'm using this hookl
        utils.checkIfUserIscurrrentLoggedUser(context, next);
    });

    /**
    * Hook for owner "role" check
    */
    DiaLinkUser.afterRemote('*.__get__followRequests', function (context, modelInstance, next) {
        //TODO: it looks like acl doesn't kick in. for now I'm using this hookl
        utils.checkIfUserIscurrrentLoggedUser(context, next);
    });

    /**
    * Hook for owner "role" check
    */
    DiaLinkUser.afterRemote('*.__create__meetings', function (context, modelInstance, next) {
        utils.checkIfUserIscurrrentLoggedUser(context, next);
    });

    /**
     * Hook to rewrite getMeetings request.
     * First it gets all the owned meetings. Then if the user is a follower, 
     * checks for all followings and add their meetings to the response.
     */
    DiaLinkUser.afterRemote('*.__get__meetings', function (context, modelInstance, next) {
        // TODO do something with this callback hell. just promise! For now im using async lib for this
        utils.checkIfUserIscurrrentLoggedUser(context, next, true);

        // get all owned meetings
        var meetings = new Set();
        var whiteListFields = ['location', 'date', 'details', 'diaLinkUserId', 'id'];
        modelInstance.forEach(function (meet) {
            var strippedMeet = {};
            whiteListFields.forEach(function (field) {
                strippedMeet[field] = meet[field];
            });
            meetings.add(strippedMeet);
        });

        var followingsPromises = [];

        // get all followings meetings.. warning: callback hell ahead
        utils.findCurrentLoggedUser(DiaLinkUser, function (err, currentLoggedUser) {
            if (currentLoggedUser.followings) {
                currentLoggedUser.followings(function (err, followings) {
                    if (err) throw err;
                    followings.forEach(function (following) {
                        followingsPromises.push(DiaLinkUser.findOne(utils.idFilter(following.id)));
                    });
                    Promise.all(followingsPromises).then(function (users) {
                        if (err) throw err;
                        async.each(users, function (user, asyncNext) {
                            if (user.meetings) {
                                user.meetings({ where: { date: { gt: new Date() + '' } } }, function (err, meetingsDb) {
                                    if (err) throw err;
                                    meetingsDb.forEach(function (meet) {
                                        var strippedMeet = {};
                                        whiteListFields.forEach(function (field) {
                                            strippedMeet[field] = meet[field];
                                            meetings.add(strippedMeet);
                                        });
                                    });
                                    asyncNext();
                                });
                            } else {
                                asyncNext();
                            }
                        }, function (err) {
                            context.result = Array.from(meetings);
                            next(err);
                        });
                    }).catch(function (err) {
                        next(err);
                    });
                });
            } else {
                next();
            }
        });
    });

    /**
     * Hook to asign a patient role if the created user is a patient
     */
    DiaLinkUser.afterRemote('create', function (context, user, next) {

        var ProfileContainer = DiaLinkUser.app.models.ProfileContainer;
        ProfileContainer.createContainer({ name: user.id.toString() }, function () {
            utils.createAvatar(user, function () {
                console.log("**Created AVATAR for user: " + user.username);
            });
        });

        if (!user.patient) {
            next();
            return;
        }
        var Role = DiaLinkUser.app.models.Role;
        var RoleMapping = DiaLinkUser.app.models.RoleMapping;
        Role.create({
            name: 'patient'
        }, function (err, role) {
            if (err) throw err;
            role.principals.create({
                principalType: RoleMapping.USER,
                principalId: user.id
            }, function (err, principal) {
                if (err) throw err;
                console.log("Created PATIENT ROLE for: " + user.username);
            });
        });
        next();
    });

    /**
     * Remote method to add a follow request
     */
    DiaLinkUser.addFollowRequest = function (patientId, response) {
        utils.findCurrentLoggedUser(DiaLinkUser, function (err, currentLoggedUser) {
            if (err) throw err;
            if (currentLoggedUser.id != patientId) {
                DiaLinkUser.findOne(utils.idFilter(patientId), function (err, patient) {
                    if (err) throw err;
                    if (patient.patient) {
                        currentLoggedUser.followings.add(patient, function (err, created) {
                            if (err) throw err;
                            patient.followers.add(currentLoggedUser, function (err, created) {
                                if (err) throw err;
                                response(null, "Follow request sent!");
                            });
                        });
                    } else {
                        response(new Error("Access denied. The user to follow must be a patient"));
                    }
                });
            } else {
                response(new Error("Access denied. You can't follow yourself"));
            }
        });
    };

    DiaLinkUser.remoteMethod('addFollowRequest', {
        accepts: { arg: 'patientId', type: utils.tableIdType },
        returns: { arg: 'addOk', type: 'string' },
        http: { verb: 'get', path: '/:patientId/addFollowRequest', errorStatus: 403 }
    });

    /**
    * Remote method to create a check in report
    */
    DiaLinkUser.createCheckInReport = function (data, response) {
        utils.findCurrentLoggedUser(DiaLinkUser, function (err, user) {
            if (err) throw err;
            user.checkInReports.create({ date: Date.now() })
                .then(function (report) {
                    return report.answers.create(data.answers);
                })
                .then(function () {
                    response(null, data);
                })
                .catch(function (err) {
                    throw err;
                });
        });

    }

    DiaLinkUser.remoteMethod('createCheckInReport', {
        accepts: { arg: 'data', type: 'object', http: { source: 'body' } },
        returns: { arg: 'reportDone', type: 'string' },
        http: { verb: 'post' }
    });

    /**
    * Remote method to create a glycemic chart data
    */
    DiaLinkUser.glycemicChartData = function (patientId, response) {
        var q = 'What was your blood sugar level at [meal time/bedtime]?';
        var filter = { where: { question: q } }
        DiaLinkUser.findOne(utils.idFilter(patientId))
            .then(function (patient) {
                patient.checkInReports(function (err, reports) {
                    var chartData = [];
                    async.each(reports, function (report, asyncNext) {
                        report.answers(function (err, arr) {
                            if (arr && arr.length > 0) {
                                var data = {
                                    gi: arr[0].answer,
                                    date: report.date
                                };
                                chartData.push(data);
                            }
                            asyncNext();
                        })
                    }, function (err) {
                        if (err) throw err;
                        response(null, chartData);
                    });
                });
            }).catch(function (err) {
                console.log(err);
                throw err;
            });
    };

    /**
     * Hook for follow "role" check
     */
    DiaLinkUser.beforeRemote('glycemicChartData', function (context, modelInstance, next) {
        utils.checkIfUserIsFollowingPatient(DiaLinkUser, context, next);
    });


    DiaLinkUser.remoteMethod('glycemicChartData', {
        accepts: { arg: 'id', type: utils.tableIdType },
        returns: { arg: 'dataChart', type: 'array' },
        http: { verb: 'get', path: '/:id/glycemicChartData' }
    });
};
