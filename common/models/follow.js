var loopback = require('loopback');
var utils = require('./../utils/utils.js')();

module.exports = function (Follow) {

    /**
     * General hook. Check if follow relationship and allow to delete the follow from one of the players
     */
    Follow.beforeRemote('**', function (context, inst, next) {
        var method = context.method.name;
        if (method === 'updateAttributes' || method === 'deleteById') {
            Follow.findOne(utils.idFilter(context.req.params.id), function (err, followEntry) {
                var loggedId = utils.getCurrentLoggedUserId();
                console.log(method +  "" + typeof loggedId +  " " + typeof followEntry.followeeId);
                if (!followEntry) {
                    utils.deny(context, next, 'Follow relation not found', 404);
                } else if (followEntry.followeeId.toString() === loggedId ||
                    (method === 'deleteById' && followEntry.followerId.toString() === loggedId)){ // removes a follow
                    next();
                } else {
                    utils.deny(context, next);
                }
            });
        } else {
            next();
        }
    });
    
    /**
     * Remote method check the follow status of the current logged user with the requested user (id)
     * Can be: anon or pending, follower, following (or a combination between last 3)
     */

    Follow.status = function (id, cb) {
        var currentLoggedUserId = utils.getCurrentLoggedUserId();
        var follower = currentLoggedUserId;
        var followee = id;
        status = []
        if (id == currentLoggedUserId) {
            status.push({ type: 'owner' })
            cb(null, status);
        } else {
            Follow.findOne({ where: { followeeId: followee, followerId: follower } }, function (err, result) {
                if (err) throw err;
                if (result)
                    status.push({ type: 'followee', pending: !result.accepted, id: result.id });
                var follower = id;
                var followee = currentLoggedUserId;
                Follow.findOne({ where: { followeeId: followee, followerId: follower } }, function (err, result) {
                    if (err) throw err;
                    if (result)
                        status.push({ type: 'follower', pending: !result.accepted, id: result.id });
                    return cb(null, status);
                });

            });
        }
    };

    Follow.remoteMethod(
        'status',
        {
            accepts: [{ arg: 'id1', type: utils.tableIdType }],
            returns: { arg: 'status', type: 'array' },
            http: { path: '/status/:id1', verb: 'get' }
        }
    );

    /**
     * Remote method the count all follow. Returns an object with number of all followings, followers and pendings
     */
    Follow.countAllFollow = function (userId, response) {
        var count = {};
        Follow.count({ followerId: userId, accepted: true }).then(function (no) {
            count.following = no;
            return Follow.count({ followeeId: userId, accepted: true });
        }).then(function (no) {
            count.follower = no;
            return Follow.count({ followeeId: userId, accepted: false })
        }).then(function (no) {
            count.pendingFollower = no;
            response(null, count);
        }).catch(function (err) {
            throw err;
        });
    }

    Follow.remoteMethod('countAllFollow', {
        accepts: { arg: 'userId', type: utils.tableIdType, required: true },
        returns: { arg: 'count', type: 'object' },
        http: {
            path: '/:userId/countAllFollow',
            verb: 'get'
        }
    });
};
