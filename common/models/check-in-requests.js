var utils = require('./../utils/utils.js')();

module.exports = function (CheckInRequests) {

    //API exposure
    CheckInRequests.disableRemoteMethod("create", true);
    CheckInRequests.disableRemoteMethod("upsert", true);
    CheckInRequests.disableRemoteMethod("updateAttributes", false)

    CheckInRequests.disableRemoteMethod("findById", true);
    CheckInRequests.disableRemoteMethod("findOne", true);

    CheckInRequests.disableRemoteMethod("deleteById", true);
    CheckInRequests.disableRemoteMethod("__get__from", false);
    CheckInRequests.disableRemoteMethod("__get__target", false);
    CheckInRequests.disableRemoteMethod("exists", true);
    CheckInRequests.disableRemoteMethod("count", true);
    CheckInRequests.disableRemoteMethod('createChangeStream', true);

    /**
     * Hook the deny to send to yourself a followrequest
     */
    CheckInRequests.beforeRemote('send', function (context, instance, next) {
        var followingId = context.req.params.id;
        if (followingId == utils.getCurrentLoggedUserId()) {
            utils.deny(context, next, 'You can not send check in requests to yourself');
        }
        utils.checkIfUserIsFollowingPatient(CheckInRequests, context, next);
    });

    /**
     * Hook to allow only the current logged user to see their follow request
     */
    CheckInRequests.beforeRemote('find', function (context, instance, next) {
        if (context.req.query.filter) {
            var filter = JSON.parse(context.req.query.filter);
            utils.checkIfUserIscurrrentLoggedUser(context, next, false, filter.where.targetId);
        } else {
            utils.deny(context, next);
        }
    });

    /**
     * Hook to allow to accept/reject follow requests just for current logged user;
     */
    CheckInRequests.beforeRemote('updateAll', function (context, instance, next) {
        if (context.req.query.where) {
            var where = JSON.parse(context.req.query.where);
            utils.checkIfUserIscurrrentLoggedUser(context, next, false, where.targetId);
        } else {
            utils.deny(context, next);
        }
    });

    /**
     * Remote method to send a follow request
     */
    CheckInRequests.send = function (followingId, cb) {
        var DiaLinkUser = CheckInRequests.app.models.DiaLinkUser;
        var _loggedUser, _followingUser;
        utils.findCurrentLoggedUser(CheckInRequests)
            .then(function (loggedUser) {
                _loggedUser = loggedUser;
                return DiaLinkUser.findOne(utils.idFilter(followingId));
            })
            .then(function (followingUser) {
                _followingUser = followingUser;
                return _loggedUser.checkInRequests.add(_followingUser);
            })
            .then(function (checkInRequests) {
                var newCount = checkInRequests.count + 1;
                return checkInRequests.updateAttributes({ "count": newCount });
            })
            .then(function (checkInRequests) {
                cb(null, "Check In Request was sent");
            })
            .catch(function (err) {
                cb(err);
            });
    };

    CheckInRequests.remoteMethod('send', {
        accepts: { arg: 'id', type: utils.tableIdType, required: true },
        returns: { arg: 'sent', type: 'string' },
        http: {
            path: '/:id/send',
            verb: 'get'
        }
    });

    /**
     * Remote method to count all follow requests. Replaces "count" which is a default api endpoint and is disabled 
     */
    CheckInRequests.kount = function (cb) {
        var id = utils.getCurrentLoggedUserId();
        CheckInRequests.find({
            where: { targetId: id },
            fields: ['count']
        }).then(function (arr) {
            var total = (arr.length > 1) ? arr.reduce(function (curr, prev) {
                return curr.count + prev.count;
            }) : (arr.length === 0) ? 0 : arr[0].count;
            cb(null, total);
        }).catch(function (err) {
            cb(err);
        });
    };

    CheckInRequests.remoteMethod('kount', {
        returns: { arg: 'count', type: 'number' },
        http: {
            path: '/kount',
            verb: 'get'
        }
    });

};
