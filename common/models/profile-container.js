var loopback = require('loopback'),
    utils = require('./../utils/utils.js')();

module.exports = function (ProfileContainer) {
    /**
     * Hook for ownership check
     */
    ProfileContainer.beforeRemote('upload', function (context, inst, next) {
        var containerId = context.req.params.container;
        if (containerId == utils.getCurrentLoggedUserId()) {// non strict equality!
            next();
        } else {
            utils.deny(context, next, "Access denied. You can't edit other user profile!");
        };
    });
};
