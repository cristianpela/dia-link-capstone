var loopback = require('loopback'),
    fs = require('fs'),
    avatarRequest = require('request');
    chance = new require('chance')();

//!!Change this to true when you want to populate an empty DB    
var POPULATE_DB = false;

function getCurrentLoggedUserId() {
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx && ctx.get('accessToken');
    var userId = accessToken && accessToken.userId;
    return userId;
}

/**
 * check if the incoming user id from request matches the current logged user id
 */
function checkIfUserIscurrrentLoggedUser(context, next, propagate, id) {
    var userIdFromUrl = id || context.req.params.id,
        currentLoggedUserId = getCurrentLoggedUserId();
    if (userIdFromUrl == currentLoggedUserId) {// we have the currrentLoggedUser [equality check is not strict! one number other is string]
        if (!propagate)
            next();
    } else {
        context.res.status(403);
        return next(new Error("Access denied."));
    }
}

/**
 * checks out if the incoming user id in a follow relation with current logged user id
 */
function checkIfUserIsFollowingPatient(Model, context, next) {
    var userFollowedId = context.req.params.id,
        currentLoggedUserId = getCurrentLoggedUserId();
    if (userFollowedId == currentLoggedUserId) {// we have the currrentLoggedUser [equality check is not strict! one number other is string]
        next();
    } else {
        var Follow = Model.app.models.Follow;
        Follow.findOne({ where: { followeeId: userFollowedId, followerId: currentLoggedUserId, accepted: true } }, function (err, found) {
            if (err) throw err;
            if (found === null) {
                context.res.status(403);
                return next(new Error("Access denied. You must follow the user first!"));
            } else
                next();
        });
    }
}

/**
 * query for current logged user instance
 */
function findCurrentLoggedUser(Model, cb) {
    var DiaLinkUser = Model.app.models.DiaLinkUser;
    if (cb)
        DiaLinkUser.findOne(idFilter(getCurrentLoggedUserId()), cb);
    else // return a promise
        return DiaLinkUser.findOne(idFilter(getCurrentLoggedUserId()));
}

/**
 * helper creator method for a filter object with [where id] pair  
 */
function idFilter(id, key) {
    var filter = {};
    var constraint = {};
    constraint[key || 'id'] = id;
    filter.where = constraint;
    return filter;
}

/**
 * helper cretor methor for a deny response;
 */
function deny(context, next, message, status) {
    context.res.status(status || 403);
    next(new Error(message || "Access denied."));
}

/**
 * creates a random lego avatar from radomuser.me
 */
function createAvatar(user, cb) {
    var legoPic = chance.natural({ min: 0, max: 9 });
    var avatarUri = "https://randomuser.me/api/portraits/lego/" + legoPic + ".jpg",
        filename = "server/storage/" + user.id + "/avatar.jpg";
    avatarRequest.head(avatarUri, function (err, res, body) {
        if (err) throw err;
        avatarRequest(avatarUri).pipe(fs.createWriteStream(filename)).on('close', cb);
    });
}

 var questions = [
        { body: 'What was your blood sugar level at [meal time/bedtime]?' },
        { body: 'What time did you check your blood sugar level at [meal time]?' },
        { body: 'What did you eat at [meal time]?' },
        { body: 'Did you administer insulin?' },
        { body: 'Who were you with when you checked/should have checked your blood sugar' },
        { body: 'Where were you when you checked/should have checked your blood sugar?' },
        { body: 'How was your mood when you checked/should have checked your blood sugar?' },
        { body: 'How was your stress level when you checked/should have checked your blood sugar? ' },
        { body: 'How was your energy level when you checked/should have checked your blood sugar?' },
        { body: ' Were any of these things happening at the time you checked/should have checked your blood sugar: Rushing; feeling tired of diabetes; feeling sick; on the road; really hungry; wanting privacy; busy and didn’t want to stop; without supplies; feeling low; feeling high; having a lot of fun; tired ' }
    ];

module.exports = function checker() {
    return {
        idFilter: idFilter,
        deny: deny,
        getCurrentLoggedUserId: getCurrentLoggedUserId,
        findCurrentLoggedUser: findCurrentLoggedUser,
        checkIfUserIscurrrentLoggedUser: checkIfUserIscurrrentLoggedUser,
        checkIfUserIsFollowingPatient: checkIfUserIsFollowingPatient,
        createAvatar: createAvatar,
        tableIdType: "string", // use string vor mongo key integer for relational dbs
        populateDb: POPULATE_DB,
        questions: questions
    }
}