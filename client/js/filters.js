angular.module('starter.filters',[])
.filter('ageFilter', function() {
     function calculateAge(birthday) {
         var ageDifMs = Date.now() - new Date(birthday).getTime();
         var ageDate = new Date(ageDifMs); // miliseconds from epoch
         return Math.abs(ageDate.getUTCFullYear() - 1970);
     }

     return function(birthdate) { 
           return calculateAge(birthdate);
     }; 
})
.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})
.filter('arrayPrinter', function(){
    return function(arr){
        return arr.join(' & ');
    }
});