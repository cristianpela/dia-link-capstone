angular.module('starter.directives', [])
    .directive('diabuttongroup', function () {
        return {
            restrict: 'E',
            scope: {
                names: '=',
                slc: '=selected',
                whenclicked: '&'
            },
            templateUrl: 'templates/directives/dia-button-group.html',
            link: function (scope, element, attrs) {
                scope.selected = scope.slc || 0;
                scope.buttonClicked = function (index) {
                    scope.selected = index;
                    scope.whenclicked({ index: index });
                };
            }
        }
    })
    .directive('diaprofile', function () {
        return {
            restrict: 'E',
            scope: {
                user: '=',
                exp: '=expanded'
            },
            templateUrl: 'templates/directives/dia-profile.html',
            controller: function ($scope) {
                $scope.expanded = $scope.exp || false;
                $scope.toggleHideAbout = function () {
                    $scope.expanded = !$scope.expanded;
                };
            }
        }
    })
    .directive('diafollow', function () {
        return {
            restrict: 'E',
            scope: {
                list: '=',
                listid: '=',
                haccept: '=hasaccept',
                hrmv: '=hasremove',
                action: '&'
            },
            templateUrl: 'templates/directives/dia-follow.html',
            controller: function ($scope, $state) {
                $scope.goToProfile = function (id) {
                    $state.go('app.profile', { id: id });
                };
                $scope.accept = function (id) {
                    $scope.action({ name: 'accept', userid: id });
                };
                $scope.remove = function (id) {
                    $scope.action({ name: 'remove', userid: id, listid: $scope.listid });
                };
            }
        }
    })
    .directive('diamodal', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/directives/dia-modal.html',
            transclude: {
                'body': '?diamodalBody',
                'head': '?diamodalHead',
                'footer': '?diamodalFooter'
            }
        }
    })