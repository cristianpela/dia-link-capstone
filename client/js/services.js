angular.module('starter.services', [])
    .service('UtilService', ['$filter', function ($filter) {
        this.findArrayEntry = function (arrObjects, key, byValue) {
            var i, entry;
            for (i = 0; i < arrObjects.length; i++) {
                if (arrObjects[i][key] === byValue) {
                    entry = arrObjects[i];
                    break;
                }
            }
            return {
                index: i,
                entry: entry
            }
        };
        this.defaultDateFormat = function (date, format) {
            return $filter('date')(new Date(date), format || 'yyyy-MM-dd mm:ss');
        };
    }])
    .service('AuthService', ['$q', '$http', 'BASE_URL', 'CurrentLoggedUser', 'UserService', function ($q, $http, BASE_URL, CurrentLoggedUser, UserService) {
        //setting request headers
        $http.defaults.headers.common.Accept = 'application/json';
        $http.defaults.headers.common['Content-Type'] = 'application/json';

        var loggedUser = {};

        var login = function (credentials) {
            return $http({
                url: BASE_URL + "/DiaLinkUsers/login",
                method: 'POST',
                data: JSON.stringify(credentials)
            }).then(function (response) {
                var body = response.data;
                //attaching auth token to the future requests
                $http.defaults.headers.common.Authorization = body.id;
                loggedUser = {
                    token: body.id,
                    username: body.username,
                    id: body.userId,
                    isPatient: body.patient
                };
                //caching current logged user with basic info
                CurrentLoggedUser.me(loggedUser);
                return UserService.userDetails(loggedUser.id);
            }).then(function (userDetails) {
                //adding more to current logged with their details
                CurrentLoggedUser.me(userDetails, true);
                return UserService.countAllFollow(loggedUser.id);
            }).then(function () {
                //adding their followings. followers and pendingFollowers
                return UserService.followers();
            }).then(function () {
                return UserService.followings();
            }).then(function () {
                return UserService.pendingFollowers();;
            }).then(function () {
                return $q.resolve(CurrentLoggedUser.me());
            });
        };

        var logout = function (isSoftLogout) {
            //remove current user cache and session
            CurrentLoggedUser.removeMe();
            CurrentLoggedUser.clear();
            loggedUser = {};
            if (isSoftLogout)//soft logout doesn't need a server confirmation 204 status
                return $q.resolve();
            return $q(function (resolve, reject) {
                $http({
                    url: BASE_URL + "/DiaLinkUsers/logout",
                    method: 'POST'
                }).then(function () {
                    //discard auth token
                    $http.defaults.headers.common.Authorization = null;
                    resolve();
                }, function (err) {
                    reject(err);
                });
            });
        };

        var register = function (data) {
            return $http.post(BASE_URL + "/DiaLinkUsers/", data);
        }

        return {
            login: login,
            logout: logout,
            loggedUser: function () {
                return loggedUser;
            },
            isPatient: function () {
                return loggedUser.patient;
            },
            register: register,
            hasToken: function () {
                if (CurrentLoggedUser.me()) {// resuming session
                    $http.defaults.headers.common.Authorization = CurrentLoggedUser.me().token;
                    return true;
                }
                return false;
            }
        }
    }])
    .service('UserService', ['$q', '$http', 'BASE_URL', 'Upload', 'AvatarPathFactory', 'CurrentLoggedUser', 'UtilService', function ($q, $http, BASE_URL, Upload, AvatarPathFactory, CurrentLoggedUser, UtilService) {

        function createFollowUrl(followType, id, accepted, skip, limit) {
            var idKey = (followType === 'follower') ? 'followeeId' : 'followerId';
            var _id = id || CurrentLoggedUser.me().id;
            var _skip = skip || 0;
            var _limit = limit || 5;
            var query = '?filter={"where":{"' + idKey + '": "' + _id + '", "accepted": ' + accepted + '},';
            query += '"include":{"relation":"' + followType + '","scope":{"fields" :["username","fullname","country","city"]}},"skip":' +
                _skip + ', "limit":' + _limit + '}';
            return encodeURI(BASE_URL + '/Follows' + query);
        }

        function createFollowData(response, followType) {
            var follows = [];
            response.data.forEach(function (follow) {
                var id = follow[followType.id],
                    rel = follow[followType.relation];
                var _follow = {
                    id: id,
                    tableId: follow.id, // this is the table entry primary key
                    avatar: AvatarPathFactory.getPath(id),
                    username: rel.username,
                    fullname: rel.fullname,
                    location: rel.city + ', ' + rel.country
                };
                follows.push(_follow);
            });
            return follows;
        }

        this.userDetails = function (id) {
            var defer = $q.defer();
            var _id = id || CurrentLoggedUser.me().id;;
            var url = BASE_URL + '/DiaLinkUsers/' + _id;
            $http.get(encodeURI(url)).then(function (response) {
                var data = response.data;
                var user = {
                    id: data.id,
                    username: data.username,
                    fullname: data.fullname,
                    birthdate: data.date,
                    location: data.city + ', ' + data.country,
                    about: data.about,
                    avatar: AvatarPathFactory.getPath(data.id),
                    isPatient: data.patient
                }
                defer.resolve(user);
            }, function (response) {
                defer.reject(response);
            });
            return defer.promise;
        };

        this.updateDetails = function (details) {
            var _id = CurrentLoggedUser.me().id;
            return $http.put(BASE_URL + '/DiaLinkUsers/' + _id, details);
        };

        this.updatePicture = function (file) {
            var _id = CurrentLoggedUser.me().id;
            var renamedFile = Upload.rename(file, 'avatar.jpg');
            return Upload.upload({
                url: BASE_URL + '/ProfileContainers/' + _id + '/upload',
                data: { file: renamedFile }
            }).then(function () {
                return Upload.base64DataUrl(file);
            });
        };

        this.findUserIdByUsername = function (username) {
            var filter = '?filter={"where":{"username":"' + username + '"}, "fields":["id"]}';
            var url = BASE_URL + '/DiaLinkUsers' + filter;
            return $http.get(encodeURI(url)).then(function (response) {
                return (response.data.length === 0)
                    ? $q.reject("User not found. Make sure you have entered a valid username.")
                    : $q.resolve(response.data[0].id);
            });
        };

        /**
         * returns an object with count of following, follower and pendingFollowers
         */
        this.countAllFollow = function (id) {
            var defer = $q.defer();
            var _id = id || CurrentLoggedUser.me().id;
            var httpPromise = $http.get(BASE_URL + '/Follows/' + _id + '/countAllFollow');
            httpPromise.then(function (response) {
                CurrentLoggedUser.allFollowCount(response.data);
                defer.resolve(CurrentLoggedUser.allFollowCount());
            });
            return defer.promise;
        };


        this.followers = function (id, skip, limit) {
            var defer = $q.defer();
            $http.get(createFollowUrl('follower', id, true, skip, limit)).then(function (response) {
                var followers = createFollowData(response, { id: 'followerId', relation: 'follower' });
                defer.resolve(CurrentLoggedUser.followers(followers));
            }, function (response) {
                defer.reject(response);
            });
            return defer.promise;
        };


        this.pendingFollowers = function (id, skip, limit) {
            var defer = $q.defer();
            $http.get(createFollowUrl('follower', id, false, skip, limit)).then(function (response) {
                var pendingFollowers = createFollowData(response, { id: 'followerId', relation: 'follower' });
                defer.resolve(CurrentLoggedUser.pendingFollowers(pendingFollowers));
            }, function (response) {
                defer.reject(response);
            });
            return defer.promise;
        };


        this.followings = function (id, skip, limit) {
            var defer = $q.defer();
            $http.get(createFollowUrl('followee', id, true, skip, limit)).then(function (response) {
                var followings = createFollowData(response, { id: 'followeeId', relation: 'followee' });
                defer.resolve(CurrentLoggedUser.followings(followings));
            }, function (response) {
                defer.reject(response);
            });
            return defer.promise;
        };

        this.addAsFollower = function (followerId) {
            return $http.get(BASE_URL + "/DiaLinkUsers/" + followerId + '/addFollowRequest');
        };

        this.acceptFollower = function (followerId) {
            var defer = $q.defer();
            var foundResult = UtilService.findArrayEntry(CurrentLoggedUser.pendingFollowers(), 'id', followerId);
            var httpPromise = $http.put(BASE_URL + "/Follows/" + foundResult.entry.tableId, { "accepted": true });
            httpPromise.then(function () {
                CurrentLoggedUser.followers().push(foundResult.entry);
                CurrentLoggedUser.pendingFollowers().splice(foundResult.index, 1);
                CurrentLoggedUser.allCrement('follower');
                CurrentLoggedUser.allCrement('pendingFollower', -1);
                CurrentLoggedUser.save();
                defer.resolve();
            }).catch(function (errResponse) {
                defer.reject(errResponse);
            });
            return defer.promise;
        };

        this.removeFollow = function (followStatusType, followId) {
            if (followStatusType === 'follower') {
                followStatusType = 'followers';
            } else if (followStatusType === 'followee') {
                followStatusType = 'followings';
            }
            var defer = $q.defer();
            var foundResult = UtilService.findArrayEntry(CurrentLoggedUser[followStatusType](), 'id', followId);
            var httpPromise = $http.delete(BASE_URL + "/Follows/" + foundResult.entry.tableId);
            httpPromise.then(function () {
                CurrentLoggedUser[followStatusType]().splice(foundResult.index, 1);
                var whichProp = (followStatusType === 'followers') ? 'follower' : 'following';
                CurrentLoggedUser.allCrement(whichProp, -1);
                defer.resolve();
            }).catch(function (errResponse) {
                defer.reject(errResponse);
            });
            return defer.promise;
        };

        this.checkFollowStatus = function (id) {
            return $http.get(BASE_URL + "/Follows/status/" + id);
        };

        this.init = function () {
            this.followers();
            this.followings();
            this.countAllFollow();
            this.pendingFollowers();
        };

    }])
    .service('CheckInService', ['$http', '$q', 'BASE_URL', 'CurrentLoggedUser', 'UtilService', function ($http, $q, BASE_URL, CurrentLoggedUser, UtilService) {
        var questions = [];
        var requestCount = 0;
        (function () {
            $http.get(BASE_URL + '/CheckInQuestions')
                .then(function (response) {
                    questions = response.data;
                });
        })();//load the questions on service creation

        this.getQuestions = function () {
            return questions;
        };

        /**
        * get reports. default is from the past month
        */
        this.getReports = function (userId, filterType) {
            var now = new Date();
            var filterTemplate = function (time) {
                return ',"where":{"date":{"gt": "' + time + '"}}';
            };
            var filter = '"order":"date DESC"';
            if (filterType === 'month') {
                filter += filterTemplate(new Date(now.setMonth(now.getMonth() - 1)));
            } else if (filterType === 'year') {
                filter = filterTemplate(new Date(now.setFullYear(now.getFullYear() - 1)));
            }
            var _userId = userId || CurrentLoggedUser.me().id;
            var url = BASE_URL + '/DiaLinkUsers/' + _userId + "/CheckInReports?filter={" + filter + "}";
            return $http.get(encodeURI(url));
        };

        this.getReportDetail = function (userId, reportId) {
            var _userId = userId || CurrentLoggedUser.me().id;
            return $http.get(BASE_URL + '/DiaLinkUsers/' + userId + '/checkInReports/' + reportId + '/answers');
        };

        this.getGlycemicChartData = function (userId) {
            var defer = $q.defer();
            var httpPromise = $http.get(BASE_URL + '/DiaLinkUsers/' + userId + '/glycemicChartData');
            httpPromise.then(function (result) {
                //char data
                var chart = {
                    labels: [],
                    data: []
                };
                //sort by date first
                result.data.dataChart.sort(function (curr, prev) {
                    return new Date(curr) - new Date(prev);
                });
                //then remap to adapt to chart data
                result.data.dataChart.forEach(function (entry) {
                    chart.labels.push(UtilService.defaultDateFormat(entry.date, 'short'));
                    chart.data.push(entry.gi);
                });
                defer.resolve(chart);
            });
            return defer.promise;
        };

        this.postReport = function (data) {
            return $http.post(BASE_URL + '/DiaLinkUsers/createCheckInReport', data);
        };

        this.sendRequest = function (id) {
            return $http.get(BASE_URL + '/CheckInRequests/' + id + '/send');
        };

        this.getRequests = function () {
            var id = CurrentLoggedUser.me().id;
            var filter = '?filter={"where":{"targetId":"' + id + '"},' +
                '"include":{"relation":"from", "scope":{"fields":["username","id","fullname"]}}}';
            var url = BASE_URL + '/CheckInRequests/' + filter;
            return $http.get(encodeURI(url));
        };

        this.acknowledgeRequests = function () {
            var id = CurrentLoggedUser.me().id;
            var where = '?where={"targetId":"' + id + '"}';
            var updateData = { count: 0 };
            var url = BASE_URL + '/CheckInRequests/update' + where;
            return $http.post(encodeURI(url), updateData);
        };

        this.countRequests = function () {
            return $http.get(BASE_URL + '/CheckInRequests/kount');
        }
    }])
    .service('SearchService', ['$q', '$rootScope', '$http', '$window', 'BASE_URL', 'AvatarPathFactory', 'CurrentLoggedUser', function ($q, $rootScope, $http, $window, BASE_URL, AvatarPathFactory, CurrentLoggedUser) {

        this.BROADCAST_CRITERIA = 'search.criteria';
        this.BROADCAST_NEW_SEARCH = 'search.new';

        //load the search criteria from storage or create a new one
        var criteria = JSON.parse($window.sessionStorage.getItem('criteria')) || {
            by: 'username', city: CurrentLoggedUser.me().location.split(',')[0]
        };

        // creteas on user object based on response data
        function createUserFromRemote(data) {
            var user = {
                id: data.id,
                avatar: AvatarPathFactory.getPath(data.id),
                username: data.username,
                fullname: data.fullname,
                location: data.city + ', ' + data.country
            };
            return user;
        }

        //update criteria, and persist it in local storage. then broadcast  the change to AppCtrl
        this.setCriteria = function (criteriaOps, isBroadcasted) {
            for (k in criteriaOps) {
                criteria[k] = criteriaOps[k];
            }
            $window.sessionStorage.setItem('criteria', JSON.stringify(criteria));
            if (isBroadcasted)
                $rootScope.$broadcast(this.BROADCAST_CRITERIA, criteria);
        }

        this.getCriteria = function () {
            return criteria;
        }

        //stringify criteria to display
        this.criteriaPrint = function () {
            var criteriaPrint = [];
            for (key in criteria) {
                criteriaPrint.push(key + " : " + criteria[key]);
            }
            return criteriaPrint.join(' | ');
        };

        this.search = function (like) {

            var city = (criteria.city) ? ',"city":"' + criteria.city + '"' : '';
            var patient = (criteria.patient === true) ? ',"patient": ' + criteria.patient : '';

            if (like !== '') {
                var params = '"where":{"' + criteria.by + '":{"like":"' + like + '"}' + patient + city + '}';
            } else {
                city = city.replace(',', '');
                if (city === '')
                    patient = patient.replace(',', '');
                var params = '"where":{' + city.replace(',', '') + patient + '}';
            }

            var countParams = params.replace('"where":', ''); // prepare params for loopback counting template
            var countUrl = BASE_URL + '/DiaLinkUsers/count?where=' + countParams;

            function paging(skip, limit) {

                var fields = ',"fields":["username", "city", "country", "fullname", "id"], "skip":' + skip + ',"limit":' + limit + '}';
                var url = BASE_URL + '/DiaLinkUsers?filter={' + params + fields;

                var defer = $q.defer();
                $http.get(encodeURI(url)).then(function (response) {
                    var searchResults = [];
                    response.data.forEach(function (rawResult) {
                        searchResults.push(createUserFromRemote(rawResult));
                    });
                    defer.resolve(searchResults);
                }).catch(function (errResponse) {
                    defer.reject(errResponse);
                });
                return defer.promise;
            }

            return {
                // count all the results
                count: function () { return $http.get(encodeURI(countUrl)) },
                // returns a page
                paging: paging
            }
        };

        this.clear = function () {
            criteria = {};
        };
    }])
    .service('MeetingService', ['$q', '$http', 'BASE_URL', 'CurrentLoggedUser', 'UtilService', 'AvatarPathFactory', function ($q, $http, BASE_URL, CurrentLoggedUser, UtilService, AvatarPathFactory) {

        this.getMeetings = function () {
            var defer = $q.defer();
            var id = CurrentLoggedUser.me().id;
            var myJoinedMeetings;
            //first getting all the joined meetings ids form Attenders table
            $http.get(encodeURI(BASE_URL + '/Attenders?filter={"where":{"userid":"' + id + '"}, "fields":["id","meetingId"]}'))
                .then(function (response) {
                    myJoinedMeetings = response.data;
                    return $http.get(encodeURI(BASE_URL + '/DiaLinkUsers/' + id
                        + '/meetings?filter={"where":{"date":{"gt":"' + new Date().toString()
                        + '"}}, "order":"date ASC"}'));
                })
                .then(function (response) {// then returs all the meetings owned of from followers which the user is joined or not
                    var meetings = response.data;
                    // map with owner data (current user or follower)
                    meetings = meetings.map(function (meet) {
                        var userid = meet.diaLinkUserId;
                        if (userid === CurrentLoggedUser.me().id) {
                            meet.fullname = CurrentLoggedUser.me().fullname;
                            meet.username = CurrentLoggedUser.me().username;
                            meet.owner = true;
                        } else {
                            var foundFollow = UtilService.findArrayEntry(CurrentLoggedUser.followings(), 'id', userid);
                            meet.fullname = foundFollow.entry.fullname;
                            meet.username = foundFollow.entry.username;
                        }
                         //if myJoinedMeetings contains current meeting id then mark it as joined
                        var foundJoin = UtilService.findArrayEntry(myJoinedMeetings, "meetingId", meet.id).entry;
                        meet.joined = angular.isDefined(foundJoin);
                        if (meet.joined) {
                            meet.attendid = foundJoin.id;
                        }
                        return meet;
                    });
                    defer.resolve(meetings);
                });
            return defer.promise;
        };

        this.getAttenders = function (meetId) {
            var defer = $q.defer();
            var filter = '?filter={"include":{"relation":"diaLinkUser", "scope":{"fields":["fullname","username"]}}}'
            var url = BASE_URL + '/Meetings/' + meetId + '/attenders' + filter;
            var loggedUsername = CurrentLoggedUser.me().username;
            $http.get(encodeURI(url)).then(function (response) {
                var attenders = response.data.map(function (attender) {
                    return {
                        fullname: attender.diaLinkUser.fullname,
                        username: attender.diaLinkUser.username
                    };
                });
                defer.resolve(attenders);
            });
            return defer.promise;
        };

        this.join = function (meetId) {
            var id = CurrentLoggedUser.me().id;
            return $http.post(BASE_URL + '/Meetings/' + meetId + '/attenders', { userid: id });
        };

        this.leave = function (meetId, attendId) {
            console.log(meetId, attendId);
            return $http.delete(BASE_URL + '/Meetings/' + meetId + '/attenders/' + attendId);
        };

        this.cancel = function (meetId) {
            return $http.delete(BASE_URL + '/Meetings/' + meetId);
        };

        this.create = function (data) {
            var id = CurrentLoggedUser.me().id;
            return $http.post(BASE_URL + '/DiaLinkUsers/' + id + '/meetings', data)
                .then(function (response) {
                    return this.join(response.data.id);
                }.bind(this));
        };
    }])
    .service('MessageService', ['$http', 'BASE_URL', 'CurrentLoggedUser', function ($http, BASE_URL, CurrentLoggedUser) {

        this.getMessages = function () {
            var url = BASE_URL + '/DiaLinkUsers/' + CurrentLoggedUser.me().id + '/messages?filter={"order":"createdAt DESC"}';
            return $http.get(encodeURI(url));
        };

        this.postMessage = function (toUserId, data) {
            data.fromId = CurrentLoggedUser.me().id;
            return $http.post(BASE_URL + '/DiaLinkUsers/' + toUserId + '/messages', data);
        };

        this.markAsRead = function (messageId) {
            return $http.put(BASE_URL + '/DiaLinkUsers/' + CurrentLoggedUser.me().id + '/messages/' + messageId, { viewed: true });
        };

        this.deleteMessage = function (messageId) {
            return $http.delete(BASE_URL + '/DiaLinkUsers/' + CurrentLoggedUser.me().id + '/messages/' + messageId);
        };

    }]);