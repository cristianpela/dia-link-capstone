angular.module('starter', ['ui.router', 'ui.bootstrap', 'ngFileUpload', 'chart.js', 'starter.services', 'starter.factories', 'starter.controllers', 'starter.filters', 'starter.directives'])
  .constant("BASE_URL", (function () {
    var hosts = {
      localhost: "http://localhost:3000/api",
      heroku: "http://dia-link.herokuapp.com/api",
      bluemix: "http://dia-link.eu-gb.mybluemix.net/api"
    }
    return hosts.bluemix;
  })())
  .constant("APP_TYPE", { // constant used in the factory layer for PopUpFactory 
    MOBILE: 1, // we are on ionic hibrid app
    WEB: 0, // we are on angular web app
    selected: 0 // can have MOBILE or WEB value
  })
  .run(function (PopUpFactory, AuthService, CurrentLoggedUser, $rootScope, $state, $location) {
    //monitoring network errors
    $rootScope.$on('web.error', function (e, error) {
      PopUpFactory.alert(error.title, error.template);
      // if error is in Client and Server Error area then do a "soft" logout (no need for a server confirmation)
      if (error.status >= 400) {
        AuthService.logout(true).then(function () {
          $state.go('login');
        });
      }
    });

    //monitoring route changings. If not auth then all redirect to login page
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      if (!AuthService.hasToken()) {
        if (toState.name !== 'register')
          $state.go('login');
      } else if (toState.name === "login" || toState.name === 'register') {
        // we can't go to login or register if we are authenticated, just redirect to home
        $state.go('app.home');
      }
    });
  })
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    // register an en error interceptor
    $httpProvider.interceptors.push('ErrorInterceptorFactory');

    // routing
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })
      .state('register', {
        url: '/register',
        templateUrl: 'templates/register.html',
        controller: 'LoginCtrl'
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })
      .state('app.search', {
        url: '/search/:like',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/search.html',
            controller: 'SearchCtrl'
          }
        }
      })
      .state('app.messages', {
        url: '/messages',
        views: {
          'menuContent': {
            templateUrl: 'templates/messages.html',
            controller: 'MessagesCtrl'
          }
        }
      })
      .state('app.events', {
        url: '/events',
        views: {
          'menuContent': {
            templateUrl: 'templates/events.html',
            controller: 'EventsCtrl'
          }
        }
      })
      .state('app.meetings', {
        url: '/meetings',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/meetings.html',
            controller: 'MeetingsCtrl'
          }
        }
      })
      .state('app.profile', {
        url: '/profile/:id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html',
            controller: 'ProfileCtrl'
          }
        },
        resolve: {// pre loading user and followStatus
          user: function (UserService, $stateParams) {
            return UserService.userDetails($stateParams.id);
          },
          followStatus: function (UserService, $stateParams) {
            return UserService.checkFollowStatus($stateParams.id);
          }
        }
      })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
  });
