angular.module('starter.factories', [])
    .factory('ErrorInterceptorFactory', ['$q', '$rootScope', function ($q, $rootScope) {
        return {
            'responseError': function (errorResponse) {
                var title, template;
                if (errorResponse.status === -1) {
                    title = "Connection Refused";
                    template = "Could not connect to server";
                } else {
                    title = errorResponse.data.error.name + '(' + errorResponse.status + ')';
                    template = errorResponse.data.error.message;
                }
                $rootScope.$broadcast('web.error', {
                    title: title,
                    template: template,
                    status: errorResponse.status
                });
                return $q.reject(errorResponse);
            }
        };
    }]).factory('AvatarPathFactory', ['BASE_URL', function (BASE_URL) {
        return {
            getPath: function (id) {
                return BASE_URL + '/ProfileContainers/' + id + '/download/avatar.jpg';
            }
        }
    }]).factory('CurrentLoggedUser', ['$window', function ($window) {

        var _pendingFollowers = [];
        var _followers = [];
        var _followings = [];

        var _me = null;

        function saveMe() {
            $window.sessionStorage.setItem('me', JSON.stringify(_me));
        }

        function getMe() {
            if (!_me && amISaved()) {
                _me = JSON.parse($window.sessionStorage.getItem('me'));
            }
            return _me;
        }

        function amISaved() {
            return $window.sessionStorage.getItem('me') !== undefined;
        }

        function removeMe() {
            _me = null;
            $window.sessionStorage.removeItem('me');
        }

        return {
            removeMe: removeMe,
            me: function (newMe, updateMe) {
                if (newMe) {
                    if (_me === null)
                        _me = newMe;
                    if (updateMe) {
                        for (key in newMe) {
                            _me[key] = newMe[key];
                        }
                    }
                    saveMe();
                }
                return getMe();
            },
            pendingFollowers: function (newData) {
                if (newData) {
                    Array.prototype.push.apply(_pendingFollowers, newData);
                    _me.pendingFollowers = _pendingFollowers;
                    saveMe();
                }
                return getMe().pendingFollowers;
            },
            followers: function (newData) {
                if (newData) {
                    Array.prototype.push.apply(_followers, newData);
                    _me.followers = _followers;
                    saveMe();
                }
                return getMe().followers;
            },
            followings: function (newData) {
                if (newData) {
                    Array.prototype.push.apply(_followings, newData);
                    _me.followings = _followings;
                    saveMe();
                }
                return getMe().followings;
            },
            allFollowCount: function (newData, op) {
                if (newData) {
                    getMe().allFollowCount = newData;
                    saveMe();
                }
                return getMe().allFollowCount.count;
            },
            allCrement: function (prop, unit) {
                unit = unit || 1;
                getMe().allFollowCount.count[prop] = getMe().allFollowCount.count[prop] + unit;
            },
            clear: function () {
                _pendingFollowers = [];
                _followers = [];
                _followings = [];
                removeMe();
            },
            save: function () {
                saveMe();
            }
        }
    }]).factory('PopUpFactory', ['APP_TYPE', '$injector', function (APP_TYPE, $injector) {
        var interface;
        if (APP_TYPE.selected === APP_TYPE.MOBILE) {
            interface = $injector.get('MobilePopUpFactory').interface;
        } else {
            interface = $injector.get('BrowserPopUpFactory').interface;
        }
        return {
            alert: interface.alert,
            confirm: interface.confirm,
            modal: interface.modal,
            show: interface.show,
            choice: interface.choice
        };
    }]).factory('BrowserPopUpFactory', ['$q', '$uibModal', function ($q, $uibModal) {

        var interface = {
            alert: angular.noop,
            modal: angular.noop,
            confirm: angular.noop,
            show: angular.noop,
            choice: angular.noop
        };


        function modalOps(title, template, templateUrl, showCancel, scope, size) {
            return {
                template: template,
                templateUrl: templateUrl,
                scope: scope,
                controller: 'ModalCtrl',
                size: size || 'sm',
                resolve: {
                    title: function () { return title || ''; },
                    showCancel: function () { return showCancel; },
                    choices: angular.noop
                }
            };
        }

        interface.alert = function (title, template) {
            var htmlTemplate = '<diamodal><diamodal-body>' + template + '</diamodal-body></diamodal>';
            var modalInstance = $uibModal.open(modalOps(title, htmlTemplate, null, false));
            return modalInstance.result;
        };

        interface.confirm = function (title, template) {
            var htmlTemplate = '<diamodal><diamodal-body>' + template + '</diamodal-body></diamodal>';
            var modalInstance = $uibModal.open(modalOps(title, htmlTemplate, null, true));
            return modalInstance.result;
        };

        interface.choice = function (title, choices, onSelected) {
            var ops = modalOps(title, null, 'templates/modal-choice.html', false);
            ops.resolve.choices = function () { return choices };
            var modalInstance = $uibModal.open(ops);

            if (!onSelected) {//we're not interested in item selection;
                return modalInstance.result;
            }

            //proccess the promise and send a callback with selected index
            modalInstance.result.then(function (selectedIndex) {
                onSelected(selectedIndex);
            });
        };

        interface.modal = function (templateUrl, scope) {
            var ops = modalOps('', null, templateUrl, false, scope, 'lg');
            var modalInstance;
            return {
                show: function () { modalInstance = $uibModal.open(ops); },
                hide: function () { if (modalInstance) modalInstance.dismiss('cancel'); }
            }
        };

        return { interface: interface };
    }]);