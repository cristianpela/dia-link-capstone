// var avatar = require('avatar-generator')({
//     convert: 'D:/Apps/ImageMagick-6.9.3-Q16/convert'
// }), 
var fs = require('fs'),
    path = require('path'),
    STORAGE_DIR = 'server\\storage',
    chance = new require('chance')(),
    avatarRequest = require('request');
utils = require('./../../common/utils/utils.js')();



function genericUser(chance) {
    var location = locations[chance.natural({ min: 0, max: locations.length - 1 })];
    return {
        username: chance.word(),
        password: '123',
        email: chance.email(),
        patient: chance.bool(),
        city: location.city,
        country: location.country,
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    }
}

function createMeeting(user, date) {
    var now = new Date();
    var meetingData = {
        city: user.city,
        date: date || new Date(now.setMonth(now.getMonth() + 1)),
        location: chance.address(),
        details: chance.paragraph({ sentences: 3 })
    }

    user.meetings.create(meetingData, function (err, meeting) {
        meeting.attenders.create({ userid: user.id }, function (err) {
            if (err) throw err;
            console.log("Created Meeting for user: " + user.username);
        });
    });
}

function createMessage(user, from) {
    var _from = from.fullname + '(' + from.username + ')';
    user.messages.create({
        from: _from,
        fromId: from.id,
        title: chance.sentence({ words: 5 }),
        body: chance.paragraph({ sentences: 5 })
    }, function (err, created) {
        if (err) throw err
        console.log('Message sent From: ' + _from + ' to: ' + user.username)
    });
}

function createReport(user, date) {

    var answers = [];

    utils.questions.forEach(function (question, index) {
        var answer;
        switch (index) {
            case 0:
                answer = chance.natural({ min: 70, max: 250 });
                break;
            case 1:
                answer = chance.hour({ twentyfour: true }) + ":" + chance.minute();
                break;
            case 2:
                answer = chance.pickone(['soup', 'pizza', 'hot-dog', 'meat', 'milk']);
                break;
            case 3:
                answer = chance.pickone(['yes', 'no']);;
                break;
            case 4:
                answer = chance.pickone(['alone', 'with my friend', 'my coworkers']);
                break;
            case 5:
                answer = chance.pickone(['at home', 'downtown', 'at work']);
                break;
            case 6:
                answer = chance.pickone(['bored', 'tired', 'happy', 'exhausted']);
                break;
            case 7:
                answer = chance.pickone(['very stressed', 'mild', 'low', 'no stress']);
                break;
            case 8:
                answer = chance.pickone(['low', 'high']);
                break;
            case 9:
                answer = chance.pickone(['Rushing', ' feeling tired of diabetes', 'feeling sick', ' on the road', 'really hungry', ' wanting privacy', ' busy and didn’t want to stop', 'without supplies', 'feeling low', 'feeling high', 'having a lot of fun', 'tired']);
                break;
            default:
                answer = chance.word();
        }
        answers.push({
            question: question.body,
            answer: answer
        });
    });


    user.checkInReports.create({ date: date || Date.now() })
        .then(function (report) {
            return report.answers.create(answers);
        })
        .then(function () {
            console.log("Created REPORT for user: " + user.username);
        })
        .catch(function (err) {
            throw err;
        });
}

var locations = [{ city: 'Timisoara', country: 'Romania' },
    { city: 'Bucharest', country: 'Romania' },
    { city: 'Budapest', country: 'Hungary' },
    { city: 'Paris', country: 'France' },
];

var users = [
    {
        username: 'foo',
        password: 'bar',
        patient: true,
        email: 'foo@bar.com',
        city: 'Timisoara',
        country: 'Romania',
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    },
    {
        username: 'jo',
        password: 'jo',
        patient: true,
        email: chance.email(),
        city: 'Timisoara',
        country: 'Romania',
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    },
    {
        username: 'bo',
        password: 'bo',
        patient: true,
        email: chance.email(),
        city: 'Timisoara',
        country: 'Romania',
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    },
    {
        username: 'mo',
        password: 'bo',
        patient: false,
        email: chance.email(),
        city: 'Budapest',
        country: 'Hungary',
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    },
    {
        username: 'bill', password: 'bill',
        patient: false,
        email: chance.email(),
        city: 'Paris',
        country: 'France',
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    },
    {
        username: 'will', password: 'will', patient: false,
        email: chance.email(),
        city: 'Timisoara',
        country: 'Romania',
        date: chance.birthday(),
        about: chance.paragraph({ sentences: 5 }),
        fullname: chance.name()
    },
];

module.exports = function (app) {
    if (!utils.populateDb) {
        console.log('Database has data and is initialized...');
        return;
    }

    console.log('Populating database...');

    var DiaLinkUser = app.models.DiaLinkUser;
    var ProfileContainer = app.models.ProfileContainer;
    var Follow = app.models.Follow;

    for (var i = 0; i < 10; i++)
        users.push(genericUser(chance));
    var asignRole = function (user) {
        if (!user.patient) {
            return;
        }
        var Role = app.models.Role;
        var RoleMapping = app.models.RoleMapping;
        Role.create({
            name: 'patient'
        }, function (err, role) {
            if (err) throw err;
            role.principals.create({
                principalType: RoleMapping.USER,
                principalId: user.id
            }, function (err, principal) {
                if (err) throw err;
                console.log("Created PATIENT ROLE for: " + user.username);
            });
        });
    };

    var MongoDB = app.dataSources.MongoDB;

    function setUpMongoDb() {
        // clear all containers first; warning if using a normal increment id keys for name
        // this could create racing condition. Ex container 5 is deleted in the same time as is created.
        // with ProfileContainer.createContainer
        ProfileContainer.getContainers(function (err, containers) {
            if (err) throw err;
            containers.forEach(function (container) {
                ProfileContainer.destroyContainer(container.name, function (err) {
                    if (err) throw err;
                });
            });
        });
        // Create all users
        DiaLinkUser.create(users, function (err, dbUsers) {
            dbUsers.forEach(function (user) {
                asignRole(user);
                ProfileContainer.createContainer({ name: user.id + "" }, function () {
                    utils.createAvatar(user, function () {
                        console.log("Created AVATAR for user: " + user.username);
                    });
                });
            }, this);

            var userJo = dbUsers[1],
                userBo = dbUsers[2],
                userWill = dbUsers[5];

            // add some follow relationship
            userJo.followers.add(userWill, function () {
                console.log("Jo has Will as follower");
                userWill.followings.add(userJo, function () {
                    // confirm follow
                    Follow.updateAll({ followeeId: userJo.id, followerId: userWill.id }, { accepted: true }, function () {
                        console.log("Will is following Jo");
                        createMessage(userJo, userWill);
                    });
                });
            });


            userBo.followers.add(userWill, function () {
                console.log("Bo has Will as follower");
                userWill.followings.add(userBo, function () {
                    Follow.updateAll({ followeeId: userBo.id, followerId: userWill.id }, { accepted: true }, function () {
                        console.log("Will is following Bo");
                    });
                });
            });


            // Jo and Bo are following eachother, both being patients
            userBo.followers.add(userJo, function () {
                console.log("Bo has Jo as follower");
                userJo.followings.add(userBo, function () {
                    Follow.updateAll({ followeeId: userBo.id, followerId: userJo.id }, { accepted: true }, function () {
                        console.log("Jo is following Bo");
                    });
                });
            });


            userJo.followers.add(userBo, function () {
                console.log("Jo has Bo as follower");
                userBo.followings.add(userJo, function () {
                    Follow.updateAll({ followeeId: userJo.id, followerId: userBo.id }, { accepted: true }, function () {
                        console.log("Bo is following Jo");
                        // now send some messages from bo to jo
                        createMessage(userJo, userBo);
                        createMessage(userJo, userBo);
                    });
                });
            });

            userJo.followers.add(dbUsers[6], function () {
                dbUsers[6].followings.add(userJo, function () {
                    console.log("Jo has follow invite from " + dbUsers[6].username);
                });
            });
            userJo.followers.add(dbUsers[7], function () {
                dbUsers[7].followings.add(userJo, function () {
                    console.log("Jo has follow invite from " + dbUsers[7].username);
                });
            });
            userJo.followers.add(dbUsers[8], function () {
                dbUsers[8].followings.add(userJo, function () {
                    console.log("Jo has follow invite from " + dbUsers[8].username);
                });
            });

            // Jo is patient create some reports
            createReport(userJo);
            createReport(userJo);
            createReport(userJo);
            createReport(userJo);
            createReport(userJo);
            createReport(userJo);

            var now = new Date();
            createReport(userJo, new Date(now.setMonth(now.getMonth() - 1)));
            createReport(userJo, new Date(now.setFullYear(now.getFullYear() - 1)));

            createReport(userBo);

            // create meettings
            createMeeting(userJo);
            createMeeting(userBo);
            createMeeting(userBo, new Date(now.setMonth(now.getMonth() - 1)));

        });
    }


    setUpMongoDb();

}