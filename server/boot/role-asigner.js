module.exports = function (app) {
    var Role = app.models.Role;
    Role.registerResolver('patient', function (role, context, cb) {
        function reject(err) {
            if (err) {
                return cb(err);
            }
            cb(null, false);
        }
        var userId = context.accessToken.userId;
         
        if (!userId) {
            console.log("rejecting anons");
            return reject(); // do not allow anonymous users
        }
       
        var DiaLinkUser = app.models.DiaLinkUser;
        DiaLinkUser.findOne({where: {id: userId}}, function (err, account) {
            cb(null, account.patient);
        });
    });
};