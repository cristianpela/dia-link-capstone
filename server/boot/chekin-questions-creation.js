var utils = require('./../../common/utils/utils.js')();

module.exports = function (app) {
    if (!utils.populateDb)
        return;
        
    var CheckInQuestion = app.models.CheckInQuestion;
    CheckInQuestion.create(utils.questions, function () {
        console.log("Check In Questions created");
    });

}